" Project: Spacegrey Vim Theme
" Inspired by the Sublime theme - https://github.com/kkga/spacegray
" Code boilerplate from nord-vim
" License: MIT

let s:timu_spacegrey_vim_version="1.0.0"
let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:sg0 = ["#2E3440", "NONE"]
let s:sg1 = ["#3B4252", 0]
let s:sg2 = ["#434C5E", "NONE"]
let s:sg3 = ["#4C566A", 8]
let s:sg4 = ["#D8DEE9", "NONE"]
let s:sg5 = ["#E5E9F0", 7]
let s:sg6 = ["#ECEFF4", 15]
let s:sg7 = ["#8FBCBB", 14]
let s:sg8 = ["#88C0D0", 6]
let s:sg9 = ["#8FA1B3", 4]
let s:sg10 = ["#5E81AC", 12]
let s:sg11 = ["#BF616A", 1]
let s:sg12 = ["#D08770", 11]
let s:sg13 = ["#EBCB8B", 3]
let s:sg14 = ["#A3BE8C", 2]
let s:sg15 = ["#B48EAD", 5]

let s:p.normal.left = [ [ s:sg1, s:sg9 ], [ s:sg5, s:sg1 ] ]
let s:p.normal.middle = [ [ s:sg5, s:sg3 ] ]
let s:p.normal.right = [ [ s:sg5, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.normal.warning = [ [ s:sg1, s:sg13 ] ]
let s:p.normal.error = [ [ s:sg1, s:sg11 ] ]

let s:p.inactive.left =  [ [ s:sg1, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.inactive.middle = g:sg_uniform_status_lines == 0 ? [ [ s:sg5, s:sg1 ] ] : [ [ s:sg5, s:sg3 ] ]
let s:p.inactive.right = [ [ s:sg5, s:sg1 ], [ s:sg5, s:sg1 ] ]

let s:p.insert.left = [ [ s:sg1, s:sg11 ], [ s:sg5, s:sg1 ] ]
let s:p.replace.left = [ [ s:sg1, s:sg15 ], [ s:sg5, s:sg1 ] ]
let s:p.visual.left = [ [ s:sg1, s:sg13 ], [ s:sg5, s:sg1 ] ]

let s:p.tabline.left = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.middle = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.right = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.tabsel = [ [ s:sg1, s:sg8 ] ]

let g:lightline#colorscheme#timu_spacegrey_vim#palette = lightline#colorscheme#flatten(s:p)
